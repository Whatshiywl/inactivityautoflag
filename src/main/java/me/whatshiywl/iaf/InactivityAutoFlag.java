package me.whatshiywl.iaf;

import java.io.File;
import java.util.logging.Logger;

import me.whatshiywl.iaf.GMHook;
import me.whatshiywl.iaf.PluginListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class InactivityAutoFlag extends JavaPlugin {

	public static Logger log;
	public static GMHook gmhook;
	public static boolean groupmanager;
	public static boolean trackonlogin;
	public static FileConfiguration players;
	public static final String LABEL = ChatColor.AQUA
			+ "[" + ChatColor.GOLD + "InactivityAutoFlag" + ChatColor.AQUA + "] ";

	@Override
	public void onEnable() {
		log = this.getLogger();
		saveDefaultConfig();
		trackonlogin = getConfig().getBoolean("trackonlogin");
		log.info("Setting player traking to " + (trackonlogin ? "login." : "logout."));
		loadPlayers();
		if(this.setupGroupManager()) {
			gmhook = new GMHook(this);
			gmhook.onPluginEnable();
			log.info("GroupManager found and hooked!");
		} else {
			log.warning("Could not find GroupManager! The plugin will not function correctly!");
		}
		getServer().getPluginManager().registerEvents(new PluginListener(this), this);
	}

	@Override
	public void onDisable() {
		log = null;
		gmhook = null;
		groupmanager = false;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player))return true;
		if((label.equalsIgnoreCase("inactivityflag") || label.equalsIgnoreCase("iaf"))){
			if(args.length > 0){
				if(args[0].equalsIgnoreCase("list")){
					Player player = (Player) sender;
					boolean groupfilter = args.length > 1;
					String group = "";
					if(groupfilter) group = args[1];
					if(!player.hasPermission("inactivityautoflag.list")){
						sender.sendMessage(ChatColor.RED + "You do not have permissions for this.");
						return true;
					}
					for(String name : players.getValues(false).keySet()){
						Player away = getServer().getPlayer(name);
						if(groupfilter && !gmhook.getGroup(away).equals(group))continue;
						if(getAwayTime(away) >= getMaxAwayTime(away)){
							player.sendMessage(ChatColor.AQUA + name + " has been inactive for " + timeLongToString(getAwayTime(away)));
						}
					}
					return true;
				}

				//Must be last command (lookup)!
				if(Bukkit.getServer().getPlayer(args[0]) != null){
					if(!sender.hasPermission("inactivityautoflag.lookup")){
						sender.sendMessage(ChatColor.RED + "You do not have permissions for this.");
						return true;
					}
					Player player = getServer().getPlayer(args[0]);
					long time = getAwayTime(player);
					if(time < 0)sender.sendMessage(ChatColor.AQUA + "This player is not being tracked yet.");
					long max = getMaxAwayTime(player);
					sender.sendMessage(ChatColor.AQUA + player.getName() + " has been offline for " + timeLongToString(time) + " milliseconds.\n" +
							 "The limit time before they get flagged is " + max + ".");
					return true;
				}
			}
			sender.sendMessage(ChatColor.RED + "This command does not exist. Type /help inactivityautoflag for help.");
		}
		return true;
	}

	private boolean setupGroupManager() {
		log.info("Looking for GroupManager...");
		groupmanager = (getServer().getPluginManager().getPlugin("GroupManager") != null);
		return groupmanager;
	}

	public long getAwayTime(Player player){
		if(!players.contains(player.getName())) return -1;
		return System.currentTimeMillis() - players.getLong(player.getName());
	}

	public long getMaxAwayTime(Player player){
		if(!getConfig().contains("groups." + gmhook.getGroup(player))) return -1;
		String timestring = getConfig().getString("groups." + gmhook.getGroup(player));
		return timeStringToLong(timestring);
	}

	public long timeStringToLong(String timestring){
		String times[] = timestring.split(":");
		long HOUR = 3600000;
		long DAY = 86400000;
		long WEEK = 604800000;
		long MONTH = 2592000000L;
		long YEAR = 31536000000L;
		long time = 0;
		for(String timestr : times){
			String rangestr = timestr.substring(timestr.length()-1);
			long range = 1;
			switch(rangestr){
			case "h":
				range = HOUR;
				break;
			case "d":
				range = DAY;
				break;
			case "w":
				range = WEEK;
				break;
			case "m":
				range = MONTH;
				break;
			case "y":
				range = YEAR;
				break;
			}
			int amount;
			if(range == 1) amount = Integer.parseInt(timestr.substring(0, timestr.length()));
			else amount = Integer.parseInt(timestr.substring(0, timestr.length()-2));
			time += amount*range;
		}
		return time;
	}

	public String timeLongToString(long timelong){
		long HOUR = 3600000;
		long DAY = 86400000;
		long WEEK = 604800000;
		long MONTH = 2592000000L;
		long YEAR = 31536000000L;
		String time = "";
		if(timelong/YEAR > 1) time += ((int)(timelong/YEAR)) + " years, ";
		timelong = timelong%YEAR;
		if(timelong/MONTH > 1) time += ((int)(timelong/MONTH)) + " months, ";
		timelong = timelong%MONTH;
		if(timelong/WEEK > 1) time += ((int)(timelong/WEEK)) + " weeks, ";
		timelong = timelong%WEEK;
		if(timelong/DAY > 1) time += ((int)(timelong/DAY)) + " days, ";
		timelong = timelong%DAY;
		if(timelong/HOUR > 1) time += ((int)(timelong/HOUR)) + " hours and ";
		timelong = timelong%HOUR;
		if(timelong > 0) time += timelong + " ms";
		return time;
	}

	public FileConfiguration getPlayers(){
		if(players == null) loadPlayers();
		return players;
	}

	private void loadPlayers(){
		File playersFile = new File(getDataFolder(), "players.yml");
		log.info("Attempting to load " + playersFile.getPath());
		try {
			if (!playersFile.exists()) playersFile.createNewFile();
			players = new YamlConfiguration();
			players.load(playersFile);
		} catch (Exception e) {
			log.severe("Failed to load players.yml: " + e);
			return;
		}
	}
}
